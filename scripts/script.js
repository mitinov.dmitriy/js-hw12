window.addEventListener("load", function () {

    let currentSlide = 0;
    let interval = setInterval(showSlide, 3000)

    const cancelBtn = document.querySelector("#cancel_button");
    const resumeBtn = document.querySelector("#resume_button");

    cancelBtn.addEventListener("click", cancelSlides);
    resumeBtn.addEventListener("click", resumeSlides);

    function resumeSlides() {
        interval = setInterval(showSlide, 3000);
    }

    function cancelSlides() {
        clearInterval(interval);
    }

    function showSlide() {
        const slides = document.querySelectorAll(".image-to-show");

        slides.forEach((sld) => {
            sld.classList.remove("active");
            sld.classList.remove("fade");
            sld.classList.add("non_active");
        })
        currentSlide++;
        if (currentSlide > slides.length) {
            currentSlide = 1;
        }
        slides[currentSlide - 1].classList.remove("non_active");
        slides[currentSlide - 1].classList.add("active");
        slides[currentSlide - 1].classList.add("fade");
    }
});
